import c from "../environment/config.js"

const $all = (selector) => document.querySelectorAll(selector)
const $ = (selector) => $all(selector)[0]

$('#sign_link').addEventListener('click', () => {
    let email = $('#email').parentElement
    if (email.style.display == "none") {
        $('.title').innerHTML = "Регистрация"
        email.style.display = "flex"
        $('.sign_link').innerHTML = "Есть аккаунт?"
    } else {
        $('.title').innerHTML = "Авторизация"
        email.style.display = "none"
        $('.sign_link').innerHTML = "Нет аккаунта?"
    }
})

$('#sign_replace').addEventListener('click', () => {
    localStorage.setItem('not_auth', 'true')
    window.location.replace('/')
})

$('.sign_btn').addEventListener('click', async () => {
    try {
        let email = $('#email').parentElement

        if (email.style.display != "none") {
            if ($('#email').value == '') {
                throw new Error('Пустой email')
            }
        }

        if ($('#login').value == '') {
            throw new Error('Пустой login')
        }

        if ($('#password').value == '') {
            throw new Error('Пустой пароль')
        }

        if (email.style.display == "none") {
            const response = await fetch(`https://test-34479-default-rtdb.firebaseio.com/users/${$('#login').value}.json`)

            const data = await response.json()

            if (data) {
                if (data.password == $('#password').value) {
                    if (localStorage.getItem('not_auth')) {
                        localStorage.removeItem('not_auth')
                    }
                    data.login = $('#login').value
                    localStorage.setItem('user', JSON.stringify(data))
                    window.location.replace('/views/main.html')
                } else {
                    throw new Error('Неправильный пароль')
                }
            } else {
                throw new Error('Такого пользователя не существует')
            }
        } else {
            const response = await fetch(`https://test-34479-default-rtdb.firebaseio.com/users/${$('#login').value}.json`)

            const data = await response.json()

            if (!data) {
                await fetch(`https://test-34479-default-rtdb.firebaseio.com/users/${$('#login').value}.json`, {
                    method: 'PUT',
                    body: JSON.stringify({
                        "email": $('#email').value,
                        "password": $('#password').value
                    })
                })

                let log = {
                    "login": $('#login').value,
                    "email": $('#email').value,
                    "password": $('#password').value
                }


                if (localStorage.getItem('not_auth')) {
                    localStorage.removeItem('not_auth')
                }
                localStorage.setItem('user', JSON.stringify(log))
                window.location.replace('/views/main.html')
            } else {
                throw new Error('Такой пользователь существует')
            }
        }
    } catch (error) {
        console.log(error.message);
        $('.alert_text').innerHTML = error.message
        $('.alert').classList.remove('none')
    }
})