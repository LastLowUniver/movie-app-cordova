function onDeviceReady() {
    // document.addEventListener("backbutton", () => {

    // }, false);

    if (localStorage.getItem('user')) {
        window.location.replace(
            `views/main.html`);
    } else {
        window.location.replace(
            `views/${localStorage.getItem('not_auth')
                ? 'main'
                : 'auth'
            }.html`);
    }
}

window.onload = () => onDeviceReady()
