// import c from '../environment/config.js'
// import '../cordova.js'

const $all = (selector) => document.querySelectorAll(selector)
const $ = (selector) => $all(selector)[0]

window.onload = async () => {



    let routerName = window.location.href.split('/')[window.location.href.split('/').length - 1].split('.')[0];

    if (routerName != "menu") {
        $('header .material-icons').addEventListener('click', () => {
            window.location.replace('/views/menu.html')
        })
    }

    // $('.header__title').innerHTML = routerName
    if (routerName == 'main') {
        await initMain()
    } else if (routerName == "movie") {
        let data = window.location.href.split('?')[window.location.href.split('?').length - 1].split('=')
        if (data.length > 1) {
            await getMovie(data[1])
        }

        $('.backbutton').addEventListener('click', () => {
            window.location.replace('/views/main.html')
        })
    } else if (routerName == 'orders') {
        await initOrders()

        $('.backbutton').addEventListener('click', () => {
            window.location.replace('/views/main.html')
        })
    } else if (routerName == "menu") {


        initMenu()

        $('.backbutton').addEventListener('click', () => {
            window.location.replace('/views/main.html')
        })
    }
}

const initMain = async () => {
    try {
        const res = await fetch(`https://test-34479-default-rtdb.firebaseio.com/movie.json`)
        const data = await res.json()

        for (let key in data) {
            const item = data[key]
            $('.main').innerHTML += `
            <div class="main_item" data-id="${key}">
                <div class="main_item_img" style="background-image: url(${item.img})"></div>
                <div class="main_item_fill">
                    <div class="main_item_content">
                        <div class="main_item_title">${item.name}</div>
                        <div class="main_item_price">${item.price} руб</div>
                    </div>
                    <div class="main_item_content">
                        <div class="main_item_timing">${item.timing} мин</div>
                        <div class="main_item_date">${item.date_at}</div>
                    </div>
                    <div class="main_item_info">
                        Режисер: ${item.director}, Год: ${item.year}
                    </div>
                </div>
            </div>
            `
        }


        $all('.main_item').forEach(elem => {
            elem.addEventListener('click', function () {
                window.location.replace('/views/movie.html?id=' + this.attributes['data-id'].value)
            })
        })

    } catch (e) {
        $('.main').innerHTML = e
    }
}

const getMovie = async (id) => {
    let isClickBuy = true

    const res = await fetch(`https://test-34479-default-rtdb.firebaseio.com/movie/${id}.json`)

    const data = await res.json()

    $('.header__title').innerHTML = data.name

    $('.movie').innerHTML = `
        <div class="movie_img" style="background-image: url(${data.img})"></div>

        <div class="movie_title">${data.name}</div>
        <div class="movie_subtitle">Информация</div>
        <div class="movie_content">
            <span>Режисер:</span>
            <span class="movie_director">${data.director}</span>
        </div>
        <div class="movie_content">
            <span>Год:</span>
            <span class="movie_director">${data.year}</span>
        </div>
        <div class="movie_content">
            <span>Хронометраж:</span>
            <span class="movie_director">${data.timing} мин</span>
        </div>
        <div class="movie_subtitle">Описание</div>
        <div class="movie_about">
            ${data.about}
        </div>

        <div class="movie_input">
            <input type="number" id="count" value="1" placeholder="Введите количество билетов">
        </div>

        <div class="movie_btn ${localStorage.getItem('not_auth')
            ? 'not-user'
            : ''
        }">${localStorage.getItem('not_auth')
            ? 'Чтобы заказать билет нужно авторизироваться'
            : data.price + ' руб | Купить билеты'
        }</div>
    `

    $('.movie_btn').addEventListener('click', async () => {
        if (localStorage.getItem('not_auth')) {
            window.location.replace('/views/auth.html')
        } else {
            if (isClickBuy) {
                data.user = JSON.parse(localStorage.getItem('user'))
                data.elemId = id
                data.countOfPrice = $('.movie_input input').value

                await fetch(`https://test-34479-default-rtdb.firebaseio.com/orders/${JSON.parse(localStorage.getItem('user')).login}.json`, {
                    method: 'POST',
                    body: JSON.stringify(data)
                })

                window.location.replace('/views/orders.html')
            } else {
                $('.movie_btn').innerHTML = `Введите количество билетов`
            }
        }
    })

    $('.movie_input input').addEventListener('input', function () {
        if (!localStorage.getItem('not_auth')) {
            if (event.target.value != "") {
                isClickBuy = true
                $('.movie_btn').innerHTML = `${data.price * +event.target.value} руб | Купить билеты`
            } else {
                isClickBuy = false
            }
        }

    })
}

const initOrders = async () => {
    const res = await fetch(`https://test-34479-default-rtdb.firebaseio.com/orders/${JSON.parse(localStorage.getItem('user')).login}.json`)

    const data = await res.json()

    if (data) {
        for (let key in data) {
            let item = data[key]
            $('.order').innerHTML += `
            <div class="order_item">
                <div class="order_img" style="background-image: url(${item.img})"></div>
                <div class="order_content">
                    <div class="order_title">${item.name}</div>
                    <div class="order_count order_standart">Взято билетов: ${item.countOfPrice}</div>
                    <div class="order_standart order_count">Общая сумма: <br> ${item.price} руб</div>
                    <div class="order_standart order_count">Дата проведения: <br> ${item.date_at}</div>
                    <div class="order_standart order_count">Хронометраж: <br> ${item.timing} мин</div>
                </div>
            </div>
            `
        }
    } else {
        $('.order').innerHTML += `<div style="margin-top: 30px; text-align: center; font-size: 1.2rem">Заказов нет</div>`
    }
}

const initMenu = () => {
    if (localStorage.getItem('user')) {
        $('.menu').innerHTML = `
        <div class="menu_title">
            ${JSON.parse(localStorage.getItem('user')).login}
        </div>

        <div class="movie_btn" id="menu_main">Главная</div>
        <div class="movie_btn" id="menu_order">Заказы</div>
        <div class="movie_btn not-user" id="menu_logout">Выйти</div>

        `

        $('#menu_main').addEventListener('click', () => window.location.replace('/views/main.html'))
        $('#menu_order').addEventListener('click', () => window.location.replace('/views/orders.html'))
        $('#menu_logout').addEventListener('click', () => {
            localStorage.removeItem('user')
            window.location.replace('/views/auth.html')
        })
    }else {
        $('.menu').innerHTML = `
        <div class="menu_title">
            Чтобы полноценно пользоваться приложением, необходимо зарегистрироваться
        </div>

        <div class="movie_btn not-user" id="menu_logout">Войти</div>
        `

        $('#menu_logout').addEventListener('click', () => {
            window.location.replace('/views/auth.html')
        })
    }
}