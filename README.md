### Приложение продажи кинобилетов

```
Приложение для продажи кинобилетов,
написанное на основе Cordova

Firebase, как back-end
```

#### Установка
```
npm install -g cordova
npm install
```

#### Запуск
#### Android
``` 
cordova platform add android
```
#### IOS
```
cordova platform add ios
```

###### Проект для Волгоградского технического университета
